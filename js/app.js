let Entrance = httpVueLoader('components/entrance.vue'),
    Index = httpVueLoader('components/index.vue'),
    Detail = httpVueLoader('components/detail.vue'),
    About = httpVueLoader('components/about.vue'),
    Gallery = httpVueLoader('components/gallery.vue'),
    Person = httpVueLoader('components/person.vue');

const routes = [
  {
    path: '/',
    name: 'entrance',
    component: Entrance,
    meta: {
      keepAlive: false,
    }
  },{
    path: '/index',
    name: 'index',
    component: Index,
    meta: {
      keepAlive: true,
    }
  },{
    path: '/detail/:pid',
    name: 'detail',
    component: Detail,
    props: true,
    meta: {
      keepAlive: false,
    }
  },{
    path: '/about',
    name: 'about',
    component: About,
    meta: {
      keepAlive: false,
    }
  },{
    path: '/gallery/:pid/',
    name: 'gallery',
    component: Gallery,
  },{
    path: '*',
    name: 'notFound',
    component: Index,
    meta: {
      keepAlive: false,
    }
  }
];

const router = new VueRouter({routes});

Vue.component('comp-header', httpVueLoader('components/header.vue'));
Vue.component('comp-footer', httpVueLoader('components/footer.vue'));
Vue.component('comp-nav', httpVueLoader('components/nav.vue'));
Vue.component('comp-pagination', httpVueLoader('components/pagination.vue'));
Vue.component('comp-loading', httpVueLoader('components/loading.vue'));

var VueMasonryPlugin = window["vue-masonry-plugin"].VueMasonryPlugin;
Vue.use(VueMasonryPlugin);

const app = new Vue({
  el: '#app', 
  router,
  watch: {
    '$route' (to, from) {
      let tname = to.name,
          fname = from.name;
      if (fname == 'entrance') {
        this.transitionName = 'fadeout';
      }else if (fname == 'index' && tname == 'detail') {
        this.transitionName = 'slidein-from-top';
      }else if (fname == 'detail' && tname == 'index') {
        
        this.transitionName = 'slidein-from-bottom';
      } else if ( fname == 'detail' && tname == 'gallery') {
        this.transitionName = 'slidein-from-right';
      } else if ( fname == 'gallery' && tname == 'detail') {
        this.transitionName = 'slideout-to-right';
      } else if (tname == 'about' || (fname == 'about' && tname == 'index')) {
        this.transitionName = 'fadein';
      } else {
        this.transitionName = null;
      }
    }
  },
  created () {
    //this.setAppHeight();
    //window.addEventListener('resize', this.setAppHeight, {passive: true});
    this.orientation = window.orientation;
    window.addEventListener('orientationchange', () => {
       if (window.orientation !== this.orientation) location.reload();
    });
  },
  destroy () {
    window.removeEventListener('resize', this.setAppHeight);
  },
  data: {
    transitionName: null,
    basicImgPath: 'https://fixer.com.tw/upload/images/',
    keepAliveInclude: ['index', 'detail'],
    isKeep: false,
    orientation: null,
  },
  methods: {
    setAppHeight () {
      document.querySelector('#app').style.height = window.innerHeight + 'px';
    },
  }
});

/*
 * 使用 Router 全域設定進行 keep-alive 控制
 * (單獨設定在頁面內更改 keep-alive include 無法成功)
 */
router.beforeEach((to, from, next) => {
  let include = app.keepAliveInclude,
      isDetail = include.indexOf('detail'),
      isGallery = include.indexOf('gallery'),
      isIndex = include.indexOf('index'); 
  
  if (from.name == 'detail' && to.name !== 'gallery') {
    if (isDetail !== -1) {
      app.keepAliveInclude.splice(isDetail, 1);
    }
    /*
    if (isGallery !== -1) {
      app.keepAliveInclude.splice(isGallery, 1);
    }
    */
  }
  /* 
  if (from.name == 'detail' && to.name == 'gallery'){
    if (isGallery == -1) {
      app.keepAliveInclude.push('gallery');
    }
  }
  */

  if (from.name == 'index') {
    if (to.name !== 'index' && to.name !== 'detail' && isIndex !== -1) {
      app.keepAliveInclude.splice(isIndex, 1);
    }
    if (to.name == 'index' && to.name == 'detail' && isIndex == -1) {
      app.keepAliveInclude.push('index'); 
    }
  }
  
  next();
});

router.afterEach((to, from) => {
  let isDetail = app.keepAliveInclude.indexOf('detail');

  if (to.name == 'detail' && isDetail == -1) {
    app.keepAliveInclude.push('detail');
  }

});

